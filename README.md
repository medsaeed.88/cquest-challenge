# cquest challenge

Hello world django application

### Setup django hellowworld application

- create virtualenv `mkvirtualenv cquestchallenge`
- install requirements `pip install -r requirements.txt`
- create django projoct `django-admin startproject`
- create django helloworld application `python manage.py startapp helloworld`
- setup [URLS](./cquestchallenge/cquestchallenge/urls.py) and [Views](./cquestchallenge/helloworld/views.py) to print Hello World 

### building the application in gitlab pipeline
Creating [docker image](./Dockerfile) and in the pipeline, 
- login to dockerhub with my personal account, token saved as a secret in gitlab.
- build the image
- push to dockerhub

### Testing the application in gitlab pipeline
Only functional test is included, due to simplicity of application
testing is mainly done by creating a docker compose with 2 services
- django app
- postgres db 

db secrets are saved as secrets in gitlab -- for simplicity only secrets for testing added but in 
real life scenario would be different secrets for different envs.

postgres db container starts then django app starts, then check if curl return "Hello World".

Other tests like unit tests can run using `python manage.py test`
### deploying the application in gitlab pipeline
deployment is done for development by commits to develop branch.

in real life scenario can be ssh to server and run docker compose or can be different 
if we use for example kubernetes. in all platforms, other steps should be performed  (e.g. configuring public endpoint and DNS).

for production deployment only done with tags.

### registering new gitlab runner
I used local docker container running on my local machine.

- create the container

`docker run -it  -v /Users/msaeed/gitlab-runner/config:/etc/gitlab-runner \
 -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner register`

add the registration token from Settings > CI/CD and expand the Runners section.

- change runner config for dind setup

`vi /Users/msaeed/gitlab-runner/config/config.toml`

```
privileged = true
volumes = ["/certs/client","/cache"]
```
- start the container and check in gitlab the runner is active.