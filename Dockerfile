FROM python:3.9.1-slim

ENV USER=cquest

RUN mkdir -p /opt/application/src
COPY . /opt/application/src
WORKDIR /opt/application/src

RUN apt-get update && \
    pip install -r requirements.txt --no-cache-dir && \
    rm -rf /var/lib/apt/lists/*

RUN adduser \
    --disabled-password \
    --gecos "" \
    "$USER"

USER "$USER"

CMD ["python", "cquestchallenge/manage.py", "runserver", "0.0.0.0:8000"]